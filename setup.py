from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

# with open("requirements.txt", "r", encoding="utf-8") as fh:
#     requirements = fh.read()

setup(
    name='justdoitalreadynow31',
    version='0.4.2',
    author="justdoitalreadynow",
    author_email="justdoitalreadynow31@gmail.com",
    description="etc",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/justdoitalreadynow31/clicktch",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    
    python_requires='>=3.6',
    install_requires=[
        'Click',
    ],
    packages = find_packages('src'),
    # install_requires = [requirements],
    
    include_package_data=True,
 
    # package_data={
    #     # If any package contains *.txt or *.rst files, include them:
    #     "cliapp": ["*.txt", "*.rst"]
    # },
    entry_points={
        'console_scripts': [
            'hello = src.clickteching:hello',
            'calcul  = src.clickteching:calc',
            'tableshow = src.prettytable.file.prtt:table',
            'showpart = cliapp.datacomm:grp'
            
        ],
    },
)