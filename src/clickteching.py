import click

@click.command()
@click.option('--count', default=1, help='Number of greetings.')
@click.option('--name', prompt='Your name',
              help='The person to greet.')
def hello(count, name):
    """Simple program that greets NAME for a total of COUNT times."""
    for x in range(count):
        click.echo(f"Hello {name}!")

@click.command()
@click.option('--param1', prompt='Enter param num. 1', help='param num.1')
@click.option('--param2', prompt='Enter param num. 2', help='param num.2')
def calc(param1,param2):
    """Calc"""
    clclated=int(param1)*int(param2)
    click.echo(clclated)



# if __name__ == '__main__':
#     hello()