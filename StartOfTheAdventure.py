import random
import sys
import pdb
import os
from time import sleep



try:
    # plhitpoints = 100
    # plstrength=5
    # pllevel = 1
    plname=input("Enter Your Name: ")
  



    def gamestart():
        """Starting screen of the game"""
        answer=int(input("Do You Want To Complete The Tutorial? \n 1 To Complete or 2 To Skip "))
        if answer == 1:
            print("To Attack type Y or y, \n to Evade type N or n, \n if your HP(HitPoints) gets to 0 you Lose \n You can encounter a chest with a better sword or potion or a enemy in it")
            proceed = int(input("Start The Game? \n 1 To Start or 2 To Quit "))
            if proceed == 1:
                game()
            else:
                print("Quiting")
                exit()

        else:
            game()

    def strengthlvl1():
        """Strength of the enemy on the 1-st level of the player"""
        strengthnumberslvl1 = [5,6,7,8,9]
        enemyhpnumberslvl1 = [10,20,30,40,50]
        strength=random.choice(strengthnumberslvl1)
        enemyhp=random.choice(enemyhpnumberslvl1)
        return strength, enemyhp

    def game():
        """Game itself"""
        plhitpoints = 100
        plstrength=5
        pllevel = 1
        plgold = 0
        playing = True
        enemiesdefeated = 0
        while playing:
            encounter = random.choice([1,2,3,4,5,6,7,8,9,10]) 
            strength, enemyhp = strengthlvl1()
            os.system('clear')
            if encounter in range(1, 9):
            #if encounter == 1 or encounter == 2 or encounter == 3 or encounter == 4 or encounter == 5 or encounter == 6 or encounter == 7 or encounter == 8  :
                print("You've encountered the enemy \n It has " + str(enemyhp) + " hitpoints and " + str(strength) + " strength \n You have " + str(plhitpoints) + " hitpoints and " + str(plstrength) + " strength")
                attackcheck = input("Do you want to start the fight? ")
                if attackcheck == "Y" or attackcheck == "y":
                    enemynotdefeated = True
                    while enemynotdefeated:
                        while plhitpoints>0 :
                            if enemyhp>0 :
                                fightattackcheck=input("Do you want to attack him? ")
                                if fightattackcheck == "Y" or fightattackcheck =="y":
                                    enemyhp = enemyhp - plstrength
                                    plhitpoints = plhitpoints-strength
                                    os.system('clear')
                                    print("It has " + str(enemyhp) + " hitpoints and " + str(strength) + " strength \n You have " + str(plhitpoints) + " hitpoints and " + str(plstrength) + " strength")
                                    continue
                                elif fightattackcheck == "N" or fightattackcheck =="n":
                                    os.system('clear')
                                    print("You've escaped the fight ")
                                    enemynotdefeated = False
                                    break
                            elif plhitpoints <= 0:
                                os.system('clear')
                                print("You Lose \n You've defeated " + str(enemiesdefeated) + " enemy/enemies")
                                startagain = input("Do you want to start again? \n Y or y to start again \n N or y to quit")
                                if startagain == "Y" or startagain =="y":
                                    game()
                                elif startagain == "N" or startagain =="n":
                                    quit()
                            elif enemyhp <= 0:
                                print("Enemy has been defeated")
                                enemiesdefeated = enemiesdefeated + 1
                                sleep(2)
                                enemynotdefeated = False
                                break
                            elif enemyhp <= 0 and plhitpoints <= 0:
                                print("You couldn't survive the last attack of the enemy")
                                os.system('clear')
                                print("You Lose \n You've defeated " + str(enemiesdefeated) + " enemy/enemies")
                                startagain = input("Do you want to start again? \n Y or y to start again \n N or y to quit")
                                if startagain == "Y" or startagain =="y":
                                    game()
                                elif startagain == "N" or startagain =="n":
                                    quit()
                        continue
                    continue
                elif attackcheck == "N" or attackcheck == "n":
                    os.system('clear')
                    continue
            elif encounter == 9:
                luck = random.choice([1,2])
                opencheck = input("You've encountered the chest. Do you want to open it? ")
                if opencheck == "Y" or opencheck == "y":
                    if luck == 1:
                        print("There was the enemy in the chest \n It has " + str(enemyhp) + " hitpoints and " + str(strength) + " strength \n You have " + str(plhitpoints) + " hitpoints \n It has rooted you, you can't escape." )
                        enemynotdefeated = True
                        while enemynotdefeated:
                            while plhitpoints>0 :
                                if enemyhp>0 :
                                    fightattackcheck=input("Do you want to attack him? ")
                                    if fightattackcheck == "Y" or fightattackcheck =="y":
                                        enemyhp = enemyhp - plstrength
                                        plhitpoints = plhitpoints-strength
                                        os.system('clear')
                                        print("It has " + str(enemyhp) + " hitpoints and " + str(strength) + " strength \n You have " + str(plhitpoints) + " hitpoints")
                                        continue
                                elif plhitpoints <= 0:
                                    os.system('clear')
                                    print("You Lose \n You've defeated " + str(enemiesdefeated) + " enemy/enemies")
                                    startagain = input("Do you want to start again? \n Y or y to start again \n N or y to quit")
                                    if startagain == "Y" or startagain =="y":
                                        game()
                                    elif startagain == "N" or startagain =="n":
                                        quit()
                                elif enemyhp <= 0:
                                    print("Enemy has been defeated")
                                    enemiesdefeated = enemiesdefeated + 1
                                    sleep(2)
                                    enemynotdefeated = False
                                    break
                                elif enemyhp <= 0 and plhitpoints <= 0:
                                    print("You couldn't survive the last attack of the enemy")
                                    sleep(2)
                                    os.system('clear')
                                    print("You Lose \n You've defeated " + str(enemiesdefeated) + " enemy/enemies")
                                    startagain = input("Do you want to start again? \n Y or y to start again \n N or y to quit")
                                    if startagain == "Y" or startagain =="y":
                                        game()
                                    elif startagain == "N" or startagain =="n":
                                        quit()
                            continue
                        continue
                    elif luck == 2:
                        luckforgoodluck = random.choice([1,2])
                        if luckforgoodluck == 1:
                            plstrength=plstrength+1
                            print("There was a better sword in the chest. Your strength has been raised to " + str(plstrength))
                            sleep(2)
                            continue
                        elif luckforgoodluck == 2:
                            plhitpoints = plhitpoints + 10
                            print("There was a potion in the chest. Your hitpoints were been raised to " + str(plhitpoints))
                            sleep(2)
                            continue
                elif opencheck == "N" or opencheck == "n":
                    continue
                continue
            continue
                   

                         

        # if playing == False:
        #     os.system('clear')
        #     print("You Lose \n You've defeated " + str(enemiesdefeated) + " enemy/enemies")
        #     startagain = input("Do you want to start again? \n Y or y to start again \n N or y to quit")
        #     if startagain == "Y" or startagain =="y":
        #         game()
        #     elif startagain == "N" or startagain =="n":
        #         quit()


    gamestart()

    if playing == False:
            os.system('clear')
            print("You Lose \n You've defeated " + str(enemiesdefeated) + " enemy/enemies")
            startagain = input("Do you want to start again? \n Y or y to start again \n N or y to quit")
            if startagain == "Y" or startagain =="y":
                game()
            elif startagain == "N" or startagain =="n":
                quit()

    raise KeyboardInterrupt
except KeyboardInterrupt:
    os.system('clear')
    print("Quiting")


#game
#game